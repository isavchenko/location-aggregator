package locations

/**
  * This class groups extra utility methods
  */
object Utils {
  /**
    * Gets array of spark job parameters in form of key=value and returns map of key/value
    */
  def parseSparkJobArgs(args: Array[String]): Map[String, String] = {
    val argumentsKeyValues = args.map(arg => arg.split('=')).map(keyValue => {
      if(keyValue.length != 2) {
        throw new IllegalArgumentException("Please check that job parameters conform expected key=value format")
      }
      (keyValue(0), keyValue(1))
    })
    argumentsKeyValues.toMap
  }
}
