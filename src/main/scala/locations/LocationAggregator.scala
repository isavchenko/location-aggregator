package locations

import java.io.{ByteArrayInputStream, InputStream, OutputStream}
import java.nio.charset.StandardCharsets

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs._
import org.apache.hadoop.io.IOUtils
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import Utils._

object LocationAggregator {
  val RegionFileName = "region.csv"
  val LocationFileName = "location.csv"
  val OutputFileName = "result.csv"
  val IntermediateResultsDirectoryName = "result_parts"

  /**
    * Entry point of spark job
    */
  def main(args: Array[String]): Unit = {
    // parse job arguments and create paths to input and output files
    val jobKeyValueArguments = parseSparkJobArgs(args)
    val workingDirectoryPath = jobKeyValueArguments.getOrElse("working_directory",
      throw new IllegalArgumentException("Please provide directory with region.csv and location.csv")
    )
    val regionFilePath = new Path(workingDirectoryPath, RegionFileName).toString
    val locationFilePath = new Path(workingDirectoryPath, LocationFileName).toString

    val spark = getOrCreateSparkSession()

    // load region.csv dataframe
    val regionDataframe = createRegionDataframe(spark, regionFilePath)

    // load location.csv dataframe
    val locationDataframe = createLocationDataFrame(spark, locationFilePath)

    // spark will output intermediate results into this directory
    val csvPartitionsFolder = new Path(workingDirectoryPath, IntermediateResultsDirectoryName).toString

    val outputDataframe = calculateOutputDataFrame(regionDataframe, locationDataframe)
    outputDataframe
      .write
      .option("header", "false")
      .mode(SaveMode.Overwrite)
      .csv(csvPartitionsFolder)

    // merge partitions files into single file
    val outputCsvHeaders = outputDataframe.schema.fields.map(field => field.name)
    val intermediateResultsDirectory = new Path(workingDirectoryPath, IntermediateResultsDirectoryName)
    val outputFileName = new Path(workingDirectoryPath, OutputFileName)

    mergeCsvPartitions(
      intermediateResultsDirectory,
      outputFileName,
      spark.sparkContext.hadoopConfiguration,
      outputCsvHeaders
    )
  }

  def getOrCreateSparkSession(): SparkSession = {
    SparkSession.builder
      .appName("Location aggregator")
      .getOrCreate
  }

  def createRegionDataframe(spark: SparkSession, filePath: String): DataFrame = {
    spark.read
      .option("header", "true")
      .schema(StructType(Seq(
        StructField("region_id", IntegerType, nullable = false),
        StructField("region_name", StringType, nullable = false)
      )))
      .csv(filePath)
  }

  def createLocationDataFrame(spark: SparkSession, filePath: String): DataFrame = {
    spark.read
      .option("header", "true")
      .schema(StructType(Seq(
        StructField("lat", DoubleType),
        StructField("long", DoubleType),
        StructField("region_id", IntegerType, nullable = false),
        StructField("date", StringType, nullable = false)
      )))
      .csv(filePath)
  }

  def calculateOutputDataFrame(regionDf: DataFrame, locationDf: DataFrame): DataFrame = {
    regionDf.join(locationDf,
      regionDf.col("region_id") === locationDf.col("region_id"))
      .groupBy(col("region_name"), col("date"))
      .count()
  }

  /**
    * When writing output to hdfs, spark outputs csv file in partitions.
    * This method merges all partitions together into one csv file.
    */
  private def mergeCsvPartitions(partitionsDir: Path, outputFile: Path, conf: Configuration, csvHeaders: Seq[String]): Unit = {
    val fs = FileSystem.get(conf)
    val out: OutputStream = fs.create(outputFile, true)

    val headerBytes = csvHeaders.mkString("", ",", System.getProperty("line.separator")).getBytes(StandardCharsets.UTF_8)
    IOUtils.copyBytes(new ByteArrayInputStream(headerBytes), out, conf, false)
    try {
      val contents: Array[FileStatus] = fs.listStatus(partitionsDir, new PathFilter {
        override def accept(path: Path): Boolean = {
          path.getName.startsWith("part-") && path.getName.endsWith(".csv")
        }
      })
      contents.sortWith((fileStatus1, fileStatus2) => fileStatus1.compareTo(fileStatus2) > 0)
      for (i <- contents.indices) {
        if (contents(i).isFile) {
          val in: InputStream = fs.open(contents(i).getPath)
          try {
            IOUtils.copyBytes(in, out, conf, false)
          } finally in.close()
        }
      }
    } finally out.close()

    fs.delete(partitionsDir, true)
  }
}
