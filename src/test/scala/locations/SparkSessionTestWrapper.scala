package locations

import org.apache.spark.sql.SparkSession
import org.scalatest.FunSpec

/**
  * Infrastructure trait to mix into test suite to get spark session available for testing
  */
trait SparkSessionTestWrapper extends FunSpec {

  lazy val spark: SparkSession = {
    SparkSession
      .builder()
      .master("local")
      .appName("Location aggregator test")
      .getOrCreate()
  }

}
