package locations

import org.scalatest.{FunSpec, Matchers}

/**
  * Test utils methods
  */
class UtilsSpec extends FunSpec with Matchers {
  it("should parse jobs arguments into key/value pair"){
    val argsMap = Utils.parseSparkJobArgs(Array("key1=value1", "key2=value2"))
    argsMap.size should equal (2)
    argsMap.getOrElse("key1", "default") should equal ("value1")
    argsMap.getOrElse("key2", "default") should equal ("value2")
  }

  it("should throw exception when arguments are wrong"){
    the [IllegalArgumentException] thrownBy Utils.parseSparkJobArgs(Array("key1"))
  }
}
