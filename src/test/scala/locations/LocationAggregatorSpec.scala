package locations

import org.scalatest.FunSpec
import org.apache.spark.sql.types._
import com.github.mrpowers.spark.fast.tests.DataFrameComparer
import org.apache.spark.sql.{DataFrame, Row}

class LocationAggregatorSpec extends FunSpec
  with SparkSessionTestWrapper
  with DataFrameComparer {

  val regionSchema = StructType(Seq(
    StructField("region_id", IntegerType, nullable = false),
    StructField("region_name", StringType, nullable = false)
  ))

  val locationSchema = StructType(Seq(
    StructField("lat", DoubleType),
    StructField("long", DoubleType),
    StructField("region_id", IntegerType, nullable = false),
    StructField("date", StringType, nullable = false)
  ))

  val expectedSchemaRegionDateCountDf = List(
    StructField("region_name", StringType, nullable = false),
    StructField("date", StringType, nullable = false),
    StructField("count", LongType, nullable = false)
  )

  val locationDf: DataFrame = spark.createDataFrame(spark.sparkContext.parallelize(Seq(
    Row(-38.19189,177.0521,1,"2019-01-17"),
    Row(-38.191333,176.339021,2,"2019-01-18"),
    Row(-38.1861,175.20416,3,"2019-01-19"),
    Row(-38.180279,175.21477,3,"2019-01-19"),
    Row(-38.17995,176.25936,2,"2019-01-20")
  )), locationSchema)

  val regionDf: DataFrame = spark.createDataFrame(spark.sparkContext.parallelize(Seq(
    Row(1, "Whakatane District"),
    Row(2, "Rotorua District"),
    Row(3, "Otorohanga District")
  )), regionSchema)


  it("should correctly calculate count of events for every region and date") {
    val actualRegionDateCountDf = LocationAggregator.calculateOutputDataFrame(regionDf, locationDf)

    val expectedRegionDateCountData = Seq(
      Row("Whakatane District", "2019-01-17", 1L),
      Row("Rotorua District","2019-01-18",1L),
      Row("Otorohanga District","2019-01-19",2L),
      Row("Rotorua District","2019-01-20",1L)
    )

    val expectedRegionDateCountDf = spark.createDataFrame(
      spark.sparkContext.parallelize(expectedRegionDateCountData),
      StructType(expectedSchemaRegionDateCountDf)
    )

    assertSmallDataFrameEquality(actualRegionDateCountDf, expectedRegionDateCountDf,
      ignoreNullable = false,
      ignoreColumnNames = false,
      orderedComparison = false
    )
  }

  it("should return empty result if regions dataframe is empty") {
    val regionDf = spark.createDataFrame(
      spark.sparkContext.emptyRDD[Row],
      StructType(regionSchema))

    val actualRegionDateCountDf = LocationAggregator.calculateOutputDataFrame(regionDf, locationDf)


    val expectedRegionDateCountDf = spark.createDataFrame(
      spark.sparkContext.emptyRDD[Row],
      StructType(expectedSchemaRegionDateCountDf)
    )

    assertSmallDataFrameEquality(actualRegionDateCountDf, expectedRegionDateCountDf,
      ignoreNullable = false,
      ignoreColumnNames = false,
      orderedComparison = false
    )
  }

  it("should return empty result if location dataframe is empty") {
    val locationDf = spark.createDataFrame(
      spark.sparkContext.emptyRDD[Row],
      StructType(locationSchema))

    val actualRegionDateCountDf = LocationAggregator.calculateOutputDataFrame(regionDf, locationDf)


    val expectedRegionDateCountDf = spark.createDataFrame(
      spark.sparkContext.emptyRDD[Row],
      StructType(expectedSchemaRegionDateCountDf)
    )

    assertSmallDataFrameEquality(actualRegionDateCountDf, expectedRegionDateCountDf,
      ignoreNullable = false,
      ignoreColumnNames = false,
      orderedComparison = false
    )
  }
}
