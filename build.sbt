name := "location_aggregator"

version := "0.1"

scalaVersion := "2.11.12"

val sparkVersion = "2.3.2"

resolvers += "Spark Packages Repo" at "http://dl.bintray.com/spark-packages/maven"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-sql" % sparkVersion % "provided",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "MrPowers" % "spark-fast-tests" % "0.17.1-s_2.11"
)

artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
  "location_aggregator.jar"
}

fork in Test := true