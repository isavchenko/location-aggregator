# Qrious assignment
## Design notes
The project counts events by region and date, and is completed using spark as it offers scalable in-memory computation which suites well for data intensive applications. 

The spark application is written in scala as it allows to reduce boilerplate when writing code in spark idiomatic functional style.
The application reads 2 csv files from persistent storage. I choose HDFS to embrace data locality it provides and speed up jobs executions.
We could potentially use S3 as datasource for spark jobs but advantage of choosing HDFS is also that one can easily test application locally.
We pull data from persistent storage using spark default connectors.
Then we join data frames and execute group by region and date and as region.csv is small we broadcast it among cluster nodes.
To avoid shuffling spark partitions to one node we then use default connector to write data to csv files which contain partitions.
We then merge all parts into one file using hdfs java library.

Ideally this application should be run on EMR cluster but when completing assignment EMR cluster wasn't available to me.

The instructions to build and run application locally are provided below.

## Project structure
Project follows standard sbt directory structure. `locations.LocationAggregator` is the main class and entry point when executing jar file on spark

## Prerequisites (software used for development)
* jdk 1.8
* sbt 1.2
* apache spark 2.3.2

## Build
Please have sbt installed on your machine.
To perform crossplatform build please navigate to root of the project and run
`sbt package`

It will generate `location_aggregator.jar` file in `target/scala-2.11/`

## Run tests
Please navigate to root of the project and run
`sbt test`

There are two types of scala tests provided - integration tests in `test/scala/locations/LocationAggregationSpec` - 
it creates spark context to test that spark DataFrame calculation is correct,
and `test/scala/locations/UtilsSpec` which is unit test for helper class. For simplicity reasons all tests are put
in the same directory and use the same style of specification - FunSpec

## Execute
To launch spark on your local machine please execute
```
${SPARK_PATH}/bin/spark-submit   
--class locations.LocationAggregator
--master local[${CPU_NUM}]   
target/scala-2.11/location_aggregator.jar working_directory=${PATH_TO_INPUT_DIRECTORY}
```

where 
* ${SPARK_PATH} - directory with spark executable
* ${CPU_NUM} - number of CPU's to run application with
* ${PATH_TO_INPUT_DIRECTORY} - path to directory with input files

